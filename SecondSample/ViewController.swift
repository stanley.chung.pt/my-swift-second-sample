//
//  ViewController.swift
//  SecondSample
//
//  Created by StanleyChung on 2017/11/10.
//  Copyright © 2017年 PentiumNetwork. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var label1: UILabel!
    
    @IBAction func button1(_ sender: Any) {
        label1.text = "Yo, it's me."
    }
    

    
    @IBAction func backgroundColorChange(_ sender: UISwitch) {
        if sender.isOn {
            view.backgroundColor = UIColor.yellow
        }else {
            view.backgroundColor = UIColor.black
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

